## Project

Methods for Design Of Experiments (DOE)

## Description

The following Sampling Technique has been presented in this project:

 1- Hammersley (Low-Discrepancy)
 1- Latin Hypercube Sampling
 2- Halton (Quasi-Random Design)
 3- Taguchi (Orthogonal)



