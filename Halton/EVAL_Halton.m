
N = 3; % Number of factors
L = 10; % Number of levels

P = haltonset(3);


figure; plot(P(1:L,1), P(1:L,2), 'o');
figure; plot(P(1:L,1), P(1:L,3), 'o');