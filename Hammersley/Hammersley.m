function H = Hammersley(N,n)

% N: Number of samples in each dimensions
% n: Number of dimensions

% Hammersley Sequence:

H(1,:)=(0:N-1)/N;
PP = primes(1000);

for i = 2:n  
  for j = 0:N-1
      J = j;
      H(i,j+1)=0;
      C=1/PP(i-1);
      while J > 0
          R0 = rem(J,PP(i-1));
          H(i,j+1) = H(i,j+1) + R0*C;
          
          C = C / PP(i-1);
          J = floor(J/PP(i-1));
      end
  end
end
