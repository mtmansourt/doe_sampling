
N = 6; % No of factors 
Q = 10;  % No of levels for each factors

TA = TaguchiArray(Q, N);

figure; plot(TA(:,1), TA(:,2),'-o')
figure; plot(TA(:,1), TA(:,3),'-o')
figure; plot(TA(:,1), TA(:,4),'-o')
figure; plot(TA(:,1), TA(:,5),'-o')
figure; plot(TA(:,1), TA(:,6),'-o')